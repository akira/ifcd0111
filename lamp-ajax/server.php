<?php 
function subirArchivo($carpeta){  
  $nombre = uniqid() . basename($_FILES['foto']['name']);  
  $destino = "/var/www/html/$carpeta/$nombre";  
  $url = basename($destino);  
  // $url = "clientes\/" . basename($destino);  
  if ( move_uploaded_file($_FILES['foto']['tmp_name'], $destino) ) { return $url;} 
  else { return 'defaultAvatar.png'; } 
}

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
// variables para entender el new mysqli()
$server = "localhost";
$user = "root";
$password = "a";
$database = "facturacion";

// guardamos en $db la conexión a la base de datos para los $db->query(), $db->prepare(), etc.
$db = new mysqli($server, $user, $password, $database);
if ( $db->connect_error){ die("Error de conexión <b> $db->connect_error </b>"); }
$db->set_charset('utf8');

if (isset($_GET['listado']) ){
    if (isset($_GET['listado']) && $_GET['listado'] == 'clientes'){
        $resp = [];
        $tabla = $db->query("select * from clientes order by apellidos, nombre");
        while ( $fila = $tabla->fetch_assoc() ){ 
            array_push($resp, $fila);
        }
        echo json_encode($resp);
    }
    if (isset($_GET['listado']) && $_GET['listado'] == 'vendedores'){
        $resp = [];
        $tabla = $db->query("select * from vendedores order by apellidos, nombre");
        while ( $fila = $tabla->fetch_assoc() ){ 
            array_push($resp, $fila);
        }
        echo json_encode($resp);
    }
    if (isset($_GET['listado']) && $_GET['listado'] == 'productos'){
        $resp = [];
        $tabla = $db->query("select * from productos order by nombre");
        while ( $fila = $tabla->fetch_assoc() ){ 
            array_push($resp, $fila);
        }
        echo json_encode($resp);
    }
    if (isset($_GET['listado']) && $_GET['listado'] == 'ventas'){
        $resp = [];
        $tabla = $db->query("select * from ventasCorrectas order by fecha desc");
        while ( $fila = $tabla->fetch_assoc() ){ 
            array_push($resp, $fila);
        }
        echo json_encode($resp);
    }
    if (isset($_GET['listado']) && $_GET['listado'] == 'productividad'){
        $resp = [];
        $tabla = $db->query("select * from productividad");
        while ( $fila = $tabla->fetch_assoc() ){ 
            array_push($resp, $fila);
        }
        echo json_encode($resp);
    }
    if (isset($_GET['listado']) && $_GET['listado'] == 'rankingVendedores'){
        $resp = [];
        $tabla = $db->query("select * from rankingVendedores");
        while ( $fila = $tabla->fetch_assoc() ){ 
            array_push($resp, $fila);
        }
        echo json_encode($resp);
    }
    if (isset($_GET['listado']) && $_GET['listado'] == 'rankingClientes'){
        $resp = [];
        $tabla = $db->query("select * from rankingClientes");
        while ( $fila = $tabla->fetch_assoc() ){ 
            array_push($resp, $fila);
        }
        echo json_encode($resp);
    }
}
else if ( isset($_POST['tabla']) && !empty($_POST['tabla']) ){
    if ($_POST['tabla'] == 'clientes'){
        if ($_POST['accion'] == 'insert'){   
            $resp->foto = subirArchivo('clientes');
            $resp->sql = "insert into clientes (apellidos, nombre, dni, telefono, avatar) values ('$_POST[apellidos]',  '$_POST[nombre]',  '$_POST[dni]',  '$_POST[telefono]', '$resp->foto') ;";
            $resp->msg = "filas insertadas: " . $db->query($resp->sql);
            echo json_encode($resp);            
        }
        if ($_POST['accion'] == 'update'){
            $resp->foto = subirArchivo('clientes');
            $resp->sql = "update clientes set apellidos='$_POST[apellidos]',  nombre='$_POST[nombre]',  dni='$_POST[dni]', telefono='$_POST[telefono]', avatar='$resp->foto' where id=$_POST[id];";
            $resp->msg = "filas actualizadas: " . $db->query($resp->sql);
            echo json_encode($resp);            
        }
        if ($_POST['accion'] == 'delete'){
            $resp->sql = "delete from clientes where id=$_POST[id];";
            $resp->msj = $db->query($resp->sql);
            echo json_encode($resp);
            
        }
    }
    if ($_POST['tabla'] == 'vendedores'){
        if ($_POST['accion'] == 'insert'){
            $resp->foto = subirArchivo('vendedores');
            $resp->sql = "insert into vendedores (apellidos, nombre, dni, telefono, avatar) values ( '$_POST[apellidos]',  '$_POST[nombre]',  '$_POST[dni]',  '$_POST[telefono]', '$resp->foto' );";
            $resp->msg = "filas insertadas: " . $db->query($resp->sql);
            echo json_encode($resp);
        }
        if ($_POST['accion'] == 'update'){
            $resp->foto = subirArchivo('vendedores');
            $resp->sql = "update vendedores set apellidos = '$_POST[apellidos]', nombre = '$_POST[nombre]',  dni = '$_POST[dni]', telefono = '$_POST[telefono]', avatar = '$resp->foto' where id = '$_POST[id]' ;";
            $resp->msg = "filas modificadas: " . $db->query($resp->sql);
            echo json_encode($resp);
        }
        if ($_POST['accion'] == 'delete'){
            $resp->sql = "delete from vendedores where id=$_POST[id];";
            $resp->msj = $db->query($resp->sql);
            echo json_encode($resp);            
        }
    }
    if ($_POST['tabla']=='productos'){
        if ($_POST['accion'] == 'insert'){
            $resp->foto = subirArchivo('productos');
            $resp->sql = "insert into productos (codigo_barras, nombre, marca, proveedor, precio, avatar) values ( '$_POST[codigo_barras]',  '$_POST[nombre]',  '$_POST[marca]',  '$_POST[proveedor]' ,  '$_POST[precio]', '$resp->foto')";
            $resp->msg = "filas insertadas: " . $db->query($resp->sql);
            echo json_encode($resp);
        }
        if ($_POST['accion'] == 'update'){
            $resp->foto = subirArchivo('productos');
            $resp->sql = "update productos set nombre = '$_POST[nombre]', marca = '$_POST[marca]', codigo_barras = '$_POST[codigo_barras]', proveedor = '$_POST[proveedor]', precio = $_POST[precio], avatar = '$resp->foto' where id = $_POST[id]";
            $resp->msg = "filas modificadas: " . $db->query($resp->sql);
            echo json_encode($resp);
        }
        if ($_POST['accion'] == 'delete'){
            $resp->sql = "delete from productos where id=$_POST[id];";
            $resp->msj = $db->query($resp->sql);
            echo json_encode($resp); 
        }
    }
    if ($_POST['tabla']=='ventas'){
        if ($_POST['accion'] == 'insert'){
            $resp->sql = "insert into ventas 
            ( vendedor, cliente, producto, fecha, cantidad, precio) 
            values 
            ( $_POST[vendedor],  $_POST[cliente],  $_POST[producto],  '$_POST[fecha]' , $_POST[cantidad], $_POST[precio])";
            $resp->msg = "filas insertadas: " . $db->query($resp->sql);
            echo json_encode($resp);
        }
        if ($_POST['accion'] == 'update'){
            $resp->sql = "update ventas set vendedor = $_POST[vendedor], cliente = $_POST[cliente], producto = $_POST[producto], fecha = '$_POST[fecha]', cantidad = $_POST[cantidad], precio = $_POST[precio] where id = $_POST[id];";
            $resp->msg = "filas actualizadas: " . $db->query($resp->sql);
            echo json_encode($resp);            
        }
        if ($_POST['accion'] == 'delete'){
            $resp->sql = "delete from ventas where id=$_POST[id];";
            $resp->msj = $db->query($resp->sql);
            echo json_encode($resp);
        }
    }
}
else if ( !empty($_GET['getProducto']) ){
    $tabla = $db->query("select * from productos where id = $_GET[getProducto]; ");
    $fila = $tabla->fetch_assoc();
    echo json_encode($fila);
}
else {
    $resp->msj = 'el server no entiende la petición';
    echo json_encode($resp);
}

