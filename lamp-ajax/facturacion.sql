-- MySQL dump 10.13  Distrib 5.5.62, for debian-linux-gnu (x86_64)
--
-- Host: server    Database: facturacion
-- ------------------------------------------------------
-- Server version	5.5.62-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `facturacion`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `facturacion` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci */;

USE `facturacion`;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apellidos` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `dni` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (4,'Pascual','Marian','1235454','2345676'),(15,'Montes Altos','Mariana','3343434','undefined'),(16,'Montes Bajos','Mariano','3343434','234234324'),(18,'Kahlo','Frida','234234324','23432432423'),(20,'Picasso','Pablo','123','456'),(27,'del Pino','Pinocho','arbolito','demadera');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `productividad`
--

DROP TABLE IF EXISTS `productividad`;
/*!50001 DROP VIEW IF EXISTS `productividad`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `productividad` (
  `factura` tinyint NOT NULL,
  `prodname` tinyint NOT NULL,
  `proveedor` tinyint NOT NULL,
  `marca` tinyint NOT NULL,
  `total` tinyint NOT NULL,
  `vendape` tinyint NOT NULL,
  `vendnombre` tinyint NOT NULL,
  `cliape` tinyint NOT NULL,
  `clinombre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_barras` int(13) DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `marca` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `proveedor` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nombre` (`nombre`),
  KEY `marca` (`marca`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,55,'Pachita','DelCampo','undefined',123.45),(5,0,'Frutilla','Campos','Perez',12.00),(7,123,'Café22','Nescafe2','Alcampo',5.00),(13,1,'Rooibos','Tea Shop','Tea Shop',20.00);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `rankingClientes`
--

DROP TABLE IF EXISTS `rankingClientes`;
/*!50001 DROP VIEW IF EXISTS `rankingClientes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `rankingClientes` (
  `idCliente` tinyint NOT NULL,
  `nombre` tinyint NOT NULL,
  `ventas` tinyint NOT NULL,
  `total` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `rankingVendedores`
--

DROP TABLE IF EXISTS `rankingVendedores`;
/*!50001 DROP VIEW IF EXISTS `rankingVendedores`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `rankingVendedores` (
  `idVendedor` tinyint NOT NULL,
  `nombre` tinyint NOT NULL,
  `ventas` tinyint NOT NULL,
  `total` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `vendedores`
--

DROP TABLE IF EXISTS `vendedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apellidos` varchar(45) COLLATE utf8_spanish2_ci DEFAULT 'sin apellidos',
  `nombre` varchar(45) COLLATE utf8_spanish2_ci DEFAULT 'sin nombre',
  `dni` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendedores`
--

LOCK TABLES `vendedores` WRITE;
/*!40000 ALTER TABLE `vendedores` DISABLE KEYS */;
INSERT INTO `vendedores` VALUES (1,'Muñoz Garzón','Domingo','654654654','765'),(2,'Marco','Ortiz','66776','123456789'),(3,'Fernandez','Marisol','999999X','5555555555'),(8,'Oli','espinaca','454546','435'),(20,'salazar','pedro','y55577872y','123123123'),(23,'Arcángel','Gabriel','123456','0987654'),(25,'Teste','Teste','111','111111'),(26,'Despentes','Virgine','2222','914546');
/*!40000 ALTER TABLE `vendedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventas` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `vendedor` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `producto` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk2_idx` (`producto`),
  KEY `fk1_idx` (`cliente`),
  KEY `fk3_idx` (`vendedor`),
  CONSTRAINT `fk_ventas_1` FOREIGN KEY (`vendedor`) REFERENCES `vendedores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_3` FOREIGN KEY (`producto`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas`
--

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
INSERT INTO `ventas` VALUES (00000000004,2,18,5,'2019-02-05',3,123.45),(00000000016,8,20,1,'2019-02-05',5,12.00),(00000000017,3,15,5,'2019-02-05',3,12.00),(00000000018,3,15,1,'2019-02-18',1,123.45),(00000000019,3,18,5,'2019-02-11',5,12.00),(00000000021,26,27,5,'2019-02-18',1,12.00),(00000000022,8,27,7,'2019-02-18',10,5.00);
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `ventasCorrectas`
--

DROP TABLE IF EXISTS `ventasCorrectas`;
/*!50001 DROP VIEW IF EXISTS `ventasCorrectas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ventasCorrectas` (
  `idVenta` tinyint NOT NULL,
  `vendedorVenta` tinyint NOT NULL,
  `vendedorNombre` tinyint NOT NULL,
  `clienteVenta` tinyint NOT NULL,
  `clienteNombre` tinyint NOT NULL,
  `idProducto` tinyint NOT NULL,
  `nombreProducto` tinyint NOT NULL,
  `fecha` tinyint NOT NULL,
  `precioProducto` tinyint NOT NULL,
  `cantidadProducto` tinyint NOT NULL,
  `totalVenta` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ventasImporte`
--

DROP TABLE IF EXISTS `ventasImporte`;
/*!50001 DROP VIEW IF EXISTS `ventasImporte`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ventasImporte` (
  `id` tinyint NOT NULL,
  `vendedor` tinyint NOT NULL,
  `cliente` tinyint NOT NULL,
  `producto` tinyint NOT NULL,
  `fecha` tinyint NOT NULL,
  `cantidad` tinyint NOT NULL,
  `precio` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Current Database: `facturacion`
--

USE `facturacion`;

--
-- Final view structure for view `productividad`
--

/*!50001 DROP TABLE IF EXISTS `productividad`*/;
/*!50001 DROP VIEW IF EXISTS `productividad`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `productividad` AS select `ventas`.`id` AS `factura`,`productos`.`nombre` AS `prodname`,`productos`.`proveedor` AS `proveedor`,`productos`.`marca` AS `marca`,(`ventas`.`cantidad` * `productos`.`precio`) AS `total`,`vendedores`.`apellidos` AS `vendape`,`vendedores`.`nombre` AS `vendnombre`,`clientes`.`apellidos` AS `cliape`,`clientes`.`nombre` AS `clinombre` from (((`ventas` join `vendedores`) join `clientes`) join `productos`) where ((`ventas`.`vendedor` = `vendedores`.`id`) and (`ventas`.`cliente` = `clientes`.`id`) and (`ventas`.`producto` = `productos`.`id`)) order by `vendedores`.`apellidos` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `rankingClientes`
--

/*!50001 DROP TABLE IF EXISTS `rankingClientes`*/;
/*!50001 DROP VIEW IF EXISTS `rankingClientes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `rankingClientes` AS select `ventas`.`cliente` AS `idCliente`,concat(`clientes`.`apellidos`,', ',`clientes`.`nombre`) AS `nombre`,count(0) AS `ventas`,sum(`ventas`.`precio`) AS `total` from (`ventas` join `clientes`) where (`ventas`.`cliente` = `clientes`.`id`) group by 1 order by sum(`ventas`.`precio`) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `rankingVendedores`
--

/*!50001 DROP TABLE IF EXISTS `rankingVendedores`*/;
/*!50001 DROP VIEW IF EXISTS `rankingVendedores`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `rankingVendedores` AS select `ventas`.`vendedor` AS `idVendedor`,concat(`vendedores`.`apellidos`,', ',`vendedores`.`nombre`) AS `nombre`,count(0) AS `ventas`,sum(`ventas`.`precio`) AS `total` from (`ventas` join `vendedores`) where (`ventas`.`vendedor` = `vendedores`.`id`) group by 1 order by sum(`ventas`.`precio`) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ventasCorrectas`
--

/*!50001 DROP TABLE IF EXISTS `ventasCorrectas`*/;
/*!50001 DROP VIEW IF EXISTS `ventasCorrectas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ventasCorrectas` AS select `ventas`.`id` AS `idVenta`,`ventas`.`vendedor` AS `vendedorVenta`,concat(`vendedores`.`apellidos`,', ',`vendedores`.`nombre`) AS `vendedorNombre`,`ventas`.`cliente` AS `clienteVenta`,concat(`clientes`.`apellidos`,', ',`clientes`.`nombre`) AS `clienteNombre`,`ventas`.`producto` AS `idProducto`,concat(`productos`.`nombre`,' (',`productos`.`marca`,')') AS `nombreProducto`,`ventas`.`fecha` AS `fecha`,`productos`.`precio` AS `precioProducto`,`ventas`.`cantidad` AS `cantidadProducto`,(`productos`.`precio` * `ventas`.`cantidad`) AS `totalVenta` from (((`ventas` join `vendedores`) join `clientes`) join `productos`) where ((`ventas`.`vendedor` = `vendedores`.`id`) and (`ventas`.`cliente` = `clientes`.`id`) and (`ventas`.`producto` = `productos`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ventasImporte`
--

/*!50001 DROP TABLE IF EXISTS `ventasImporte`*/;
/*!50001 DROP VIEW IF EXISTS `ventasImporte`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ventasImporte` AS select `ventas`.`id` AS `id`,`ventas`.`vendedor` AS `vendedor`,`ventas`.`cliente` AS `cliente`,`ventas`.`producto` AS `producto`,`ventas`.`fecha` AS `fecha`,`ventas`.`cantidad` AS `cantidad`,`ventas`.`precio` AS `precio` from `ventas` order by (`ventas`.`cantidad` * `ventas`.`precio`) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07 20:58:43
