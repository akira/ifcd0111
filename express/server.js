

function insertFactura(req, res){
  db.query('start transaction');
  db.query(`insert into facturas (idcliente, fecha, total) values ( ?, ?, ? )`,
    [req.body.cliente.id, req.body.fecha, 0],
    (err, data) => { 
      if (err) { db.query('rollback'); return false }
      else {
        let factura = data.insertId;
        req.body.lineas.forEach( linea => { 
          db.query('insert into lineas (idfacturas, idproductos, nombreproducto, precioproducto, cantidadproducto, ivaproducto) values ( ?, ?, ?, ?, ?, ?)',[factura, linea.id, linea.nombre, linea.precio, linea.cantidad, linea.iva], (err, data) => {
            if (err) { db.query('rollback'); return false }
          });
        });
        db.query('commit');
        res.send({ ok: true, factura: factura });
        return true
      }
    }
  );
}



const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mysql = require('mysql');
const formidable = require('formidable');
app.use( express.static('public') );
app.use( bodyParser.json() );  
app.use( bodyParser.urlencoded() );  
app.use( (req, res, next) => {
  res.header("Content-Type", "application/json; charset=utf-8");  
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


var db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'a',
    database: 'facturanode',
    charset: "utf8"
});

app.get('/fact/:tabla', function (req, res) { 
  db.query('select * from ' + req.params.tabla, [], (err, data) => {
    console.log(data)
    res.send(data)
  });
});

app.get('/modelos', (req, res) => {
  db.query('select distinct modelo from productos', [], (err, data) => {
    res.send(data)
  })
});

app.post('/modelos', (req, res) => {
  db.query('select * from productos where modelo = ?', [req.body.modelo], (err, data) => {
    res.send(data)
  })
});

app.get('/:tabla/:id', (req, res) => {
  db.query(' select * from ?? where id = ? ', 
    [req.params.tabla, req.params.id], 
    (err, data) => {
      res.send(data[0]);
  })
});

app.post('/newFactura', insertFactura)

app.post('/clientes', (req, res) => {
  form = new formidable.IncomingForm(),
  form.parse(req, (err, fields, files) => {
    // db.query('insert into clientes (cif, cliente, direccion, telefono) values (?, ?, ?, ?)', [fields.cif, fields.cliente, fields.direccion, fields.telefono])
    console.log('grabando... ', fields);
    res.send(fields)
  })
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
