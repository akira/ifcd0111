/*
crontab:
* * * * *  /usr/local/bin/node /var/www/html/ifcd0111/actualizaDB.js > /var/www/html/ifcd0111/database.json

dependencias: curl
*/

const os = require('os');
const fs = require('fs');
const dns = require('dns');
const https = require('https');
const execSync = require('child_process').execSync;

var server = process.argv[2] == 'aula' ? {d1: 'aufs', d2: '/dev/sdb2', lan: 'eth0'} : {d1: '/dev/sda1', d2: '/dev/sda5', lan: 'eth1'};

const rootPart = server.d1, 
      homePart = server.d2;

var database = {}

function getPartition(device){
  return execSync('df -h | grep ' + device ).toString().replace(/\s\s+/g, " ").split(' ');
}

function uptime() {
  return execSync('uptime').toString();
}

function lsUsb() {
  // execSync('lsusb > ' + lsUsbFile );
  return execSync('lsusb').toString();
}

function backEnd() {
  return execSync('cat ' + process.argv[1]).toString();
}

function extIP() {
  return JSON.parse( execSync('curl "https://api.ipify.org?format=json"').toString() )
}

function nmapList() {
  return execSync('nmap localhost').toString();
}

function logSize() {
  return execSync('du -sh /var/log /var/cache/apt/archives').toString()
}

database = {
  fecha: new Date().toString(),
  uptime: uptime(),
  totalMem: os.totalmem(),
  freeMem: os.freemem(),
  root: getPartition(rootPart),
  home: getPartition(homePart),  
  iface: os.networkInterfaces()[server.lan][0],
  numCPUs: os.cpus().length,
  modelCPUs: os.cpus()[0].model,
  dns: dns.getServers(),
  usb: lsUsb(),
  backEnd: backEnd(),  
  extIP: '0.0.0.0', // extIP()
  nmap: nmapList(),
  logSize: logSize()
}
console.log(JSON.stringify(database));
// fs.writeFileSync(dbFile, JSON.stringify(database) );
