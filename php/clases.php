<?php
$db = new mysqli('192.168.105.75', 'root', 'a', 'domingo');
if ( $db->connect_error){ die("Error de conexión <b> $db->connect_error </b>"); }
$db->set_charset('utf8');

class Venta {
  function __construct($id){

    global $db;

    if ( !empty($id) ){ 

      $this->id = $id; 
      $tabla = $db->query("select * from ventas where id = '$id'");
      $fila = $tabla->fetch_assoc();
      $this->vendedor = $fila['vendedor'];  
      $this->cliente = $fila['cliente'];
      $this->fecha = $fila['fecha'];
      $this->importe = $fila['importe'];

    } else { 

      $this->id = uniqid(); 
      $this->vendedor = '';
      $this->cliente = '';
      $this->fecha = date('Y-m-d');
      $this->importe = 0;
    } 
    
   
  }

  function editWithForm($form){
    include "$form";   
  }

  function saveToDb(){
    global $db;
    $tabla = $db->query("select * from ventas where id = '$this->id'");

    if ($fila = $tabla->fetch_assoc()){
      $db->query("update ventas set vendedor = '$this->vendedor', cliente = '$this->cliente', fecha = '$this->fecha', importe = $this->importe  where id = '$this->id' ;");          
    } else {      
      $db->query("insert into ventas (id, vendedor, cliente, fecha, importe ) 
      values ('$this->id', '$this->vendedor', '$this->cliente', '$this->fecha', $this->importe);");          
    }
  }

  function deleteFromDb(){
    global $db;
    $db->query("delete from ventas where id = '$this->id'");
  }

  static function listDb(){
    global $db;
    $tabla = $db->query("select * from ventas order by fecha;");
    // while ( $fila = $tabla->fetch_assoc() ){ include 'fila.php'; }
    return $tabla;
  }

  static function rankingVendedores(){
    global $db;
    $tabla = $db->query("
    select vendedor, count(*) as ventas, sum(importe) as totalVentas
    from ventas 
    group by vendedor
    order by 3 desc;");
    return $tabla;
  }

  static function ventasVendedor($vendedor){    
    global $db;
    $tabla = $db->query("select * from ventas where vendedor = '$vendedor' order by fecha desc;");    
    return $tabla;
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Ventas App</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
  <div class="navbar-header">
  <a class="navbar-brand" type="text" href="index.php">Aplicación de ventas</a>
  </div>
  <ul class="nav navbar-nav">
  <li class="active "><a href=""><span class="glyphicon glyphicon-pencil"></span> Agregar venta </a></li>
  <li class="active "><a href="listados.php"> ventas por fecha </a></li>
  <li class="active "><a href="listados.php?option=rankingVendedor"> ranking vendedores </a></li>

  </ul>
  </div>
</nav>


