<?php
include 'clases.php';

if ( isset($_GET['id']) && !empty($_GET['id']) ){
  $v = new Venta($_GET['id']);
  $v->vendedor = $_GET['vendedor'];
  $v->cliente = $_GET['cliente'];
  $v->fecha = $_GET['fecha'];
  $v->importe = $_GET['importe'];
  $v->saveToDb();
  echo '<a href="index.php"> volver </a>';
  
} else {
  $v = new Venta($_GET['edit']);
  $v->editWithForm('editForm.php');   
}