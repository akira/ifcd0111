<?php

include 'clases.php';

//funciones de apoyo:
function ventasTotales(){
  $tabla = Venta::listDb();
  while ( $fila = $tabla->fetch_assoc() ){
    ?>
    <div class="container">
      <a href="edit.php?edit=<?= $fila['id'] ?>"> <?= $fila['id'] ?> </a>
      <?= $fila['fecha'] ?> - <?= $fila['importe'] ?>: <?= $fila['vendedor'] ?> - <?= $fila['cliente'] ?> <br>
    </div>
    <?php
  }
}

function rankingVendedores(){
  $tabla = Venta::rankingVendedores();
  while ( $fila = $tabla->fetch_assoc() ){
    ?>
    <div class="container">
      <a href="listados.php?vendedor=<?= $fila['vendedor'] ?>"> <?= $fila['vendedor'] ?> </a>: 
      <?= $fila['totalVentas'] ?> € (total de <?= $fila['ventas'] ?> ventas)
    </div>
    <?php
  }
}

function ventasVendedor(){
  $tabla = Venta::ventasVendedor($_GET['vendedor']);
  while ( $fila = $tabla->fetch_assoc() ){
    ?>
    <div class="container">
      <?= $fila['fecha'] ?> - <?= $fila['importe'] ?>: <?= $fila['id'] ?> - <?= $fila['vendedor'] ?> - <?= $fila['cliente'] ?> <br>
    </div>
    <?php
  }
}

// manejo de opciones:
if ( $_GET['option'] == 'rankingVendedor' ){
  rankingVendedores();
}
else if ( isset($_GET['vendedor']) && !empty($_GET['vendedor']) ){
  ventasVendedor();
}
else {
  ventasTotales();
}